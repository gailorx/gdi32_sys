#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

#![feature(libc)]
extern crate libc;
extern crate win32_types;

use libc::{c_int, c_short};
use win32_types::{HDC, BOOL, HBITMAP, HGDIOBJ, UINT, DWORD, WORD, BYTE, HFONT, WCHAR, LPWSTR, LPCWSTR};

pub mod PFD {
    use win32_types::{DWORD, BYTE};

    pub const TYPE_RGBA: BYTE = 0;
    pub const TYPE_COLORINDEX: BYTE = 1;
    pub const MAIN_PLANE: BYTE = 0;
    pub const OVERLAY_PLANE: BYTE = 1;
    pub const UNDERLAY_PLANE: BYTE = (-1);
    pub const DOUBLEBUFFER: DWORD = 0x00000001;
    pub const STEREO: DWORD = 0x00000002;
    pub const DRAW_TO_WINDOW: DWORD = 0x00000004;
    pub const DRAW_TO_BITMAP: DWORD = 0x00000008;
    pub const SUPPORT_GDI: DWORD = 0x00000010;
    pub const SUPPORT_OPENGL: DWORD = 0x00000020;
    pub const GENERIC_FORMAT: DWORD = 0x00000040;
    pub const NEED_PALETTE: DWORD = 0x00000080;
    pub const NEED_SYSTEM_PALETTE: DWORD = 0x00000100;
    pub const SWAP_EXCHANGE: DWORD = 0x00000200;
    pub const SWAP_COPY: DWORD = 0x00000400;
    pub const SWAP_LAYER_BUFFERS: DWORD = 0x00000800;
    pub const GENERIC_ACCELERATED: DWORD = 0x00001000;
    pub const SUPPORT_COMPOSITION: DWORD = 0x00008000;
    pub const DEPTH_DONTCARE: DWORD = 0x20000000;
    pub const DOUBLEBUFFER_DONTCARE: DWORD = 0x40000000;
    pub const STEREO_DONTCARE: DWORD = 0x80000000;
}

#[repr(C)]
pub struct PIXELFORMATDESCRIPTOR {
    pub nSize: WORD,
    pub nVersion: WORD,
    pub dwFlags: DWORD,
    pub iPixelType: BYTE,
    pub cColorBits: BYTE,
    pub cRedBits: BYTE,
    pub cRedShift: BYTE,
    pub cGreenBits: BYTE,
    pub cGreenShift: BYTE,
    pub cBlueBits: BYTE,
    pub cBlueShift: BYTE,
    pub cAlphaBits: BYTE,
    pub cAlphaShift: BYTE,
    pub cAccumBits: BYTE,
    pub cAccumRedBits: BYTE,
    pub cAccumGreenBits: BYTE,
    pub cAccumBlueBits: BYTE,
    pub cAccumAlphaBits: BYTE,
    pub cDepthBits: BYTE,
    pub cStencilBits: BYTE,
    pub cAuxBuffers: BYTE,
    pub iLayerType: BYTE,
    pub bReserved: BYTE,
    pub dwLayerMask: DWORD,
    pub dwVisibleMask: DWORD,
    pub dwDamageMask: DWORD,
}
impl Copy for PIXELFORMATDESCRIPTOR {}

#[repr(C)]
pub struct DISPLAY_DEVICEW {
  pub cb: DWORD,
  pub DeviceName: [WCHAR; 32],
  pub DeviceString: [WCHAR; 128],
  pub StateFlags: DWORD,
  pub DeviceID: [WCHAR; 128],
  pub DeviceKey: [WCHAR; 128],
}
impl Copy for DISPLAY_DEVICEW {}

#[repr(C)]
pub struct DEVMODE {
    pub dmDeviceName: [WCHAR; 32],
    pub dmSpecVersion: WORD,
    pub dmDriverVersion: WORD,
    pub dmSize: WORD,
    pub dmDriverExtra: WORD,
    pub dmFields: DWORD,
    pub union1: [u8; 16],
    pub dmColor: c_short,
    pub dmDuplex: c_short,
    pub dmYResolution: c_short,
    pub dmTTOption: c_short,
    pub dmCollate: c_short,
    pub dmFormName: [WCHAR; 32],
    pub dmLogPixels: WORD,
    pub dmBitsPerPel: DWORD,
    pub dmPelsWidth: DWORD,
    pub dmPelsHeight: DWORD,
    pub dmDisplayFlags: DWORD,
    pub dmDisplayFrequency: DWORD,
    pub dmICMMethod: DWORD,
    pub dmICMIntent: DWORD,
    pub dmMediaType: DWORD,
    pub dmDitherType: DWORD,
    pub dmReserved1: DWORD,
    pub dmReserved2: DWORD,
    pub dmPanningWidth: DWORD,
    pub dmPanningHeight: DWORD,
}
impl Copy for DEVMODE {}

#[link(name = "gdi32")]
extern "stdcall" {

    pub fn ChoosePixelFormat(hdc: HDC, ppfd: *const PIXELFORMATDESCRIPTOR) -> c_int;

    pub fn SetPixelFormat(hdc: HDC, iPixelFormat: c_int,
        ppfd: *const PIXELFORMATDESCRIPTOR) -> BOOL;

    pub fn DescribePixelFormat(hdc: HDC, iPixelFormat: c_int, nBytes: UINT,
        ppfd: *mut PIXELFORMATDESCRIPTOR) -> c_int;

    pub fn SwapBuffers(hdc: HDC) -> BOOL;

    pub fn CreateCompatibleDC(hdc: HDC) -> HDC;

    pub fn DeleteDC(hdc: HDC) -> BOOL;

    pub fn CreateCompatibleBitmap(hdc: HDC, nWidth: c_int, nHeight: c_int) -> HBITMAP;

    pub fn SelectObject(hdc: HDC, hgdiobj: HGDIOBJ) -> HGDIOBJ;

    pub fn DeleteObject(hObject: HGDIOBJ) -> BOOL;

    pub fn BitBlt(hdcDest: HDC, nXDest: c_int, nYDest: c_int, nWidth: c_int, nHeight: c_int,
                  hdcSrc: HDC, nXSrc: c_int, nYSrc: c_int, dwRop: DWORD) -> BOOL;

    pub fn CreateFontW(
        height: c_int, width: c_int, escapement: c_int, orientation: c_int,
        weight: c_int, italic: DWORD, underline: DWORD, strikeOut: DWORD,
        charSet: DWORD, outputPrecision: DWORD, clipPrecision: DWORD,
        quality: DWORD, pitchAndFamily: DWORD, face: LPCWSTR
    ) -> HFONT;

    pub fn TextOutW(hdc: HDC, nXStart: c_int, nYStart: c_int, lpString: LPWSTR, cchString: c_int) -> BOOL;
}
